import { Elysia, t } from "elysia";

new Elysia()
   .get('/', () => 'Hello World')
   .get('/json', () => ({
       hello: 'world'
   }))
   .post('/profile', ({body}) => body,
   {
       body: t.Object({
           username: t.String()
       })
   })
   .get('/id/:id', ({ params: { id } }) => id, {
    params: t.Object({
        id: t.Numeric()
    })
    })
    .delete('/resource/:id', ({ params: { id } }) => `Deleted resource with ID ${id}`, {
        params: t.Object({
            id: t.Numeric()
        })
    })
    .put('/resource/:id', ({ params: { id }, body }) => ({
        message: `Updated resource with ID ${id}`,
        data: body
    }), {
        params: t.Object({
            id: t.Numeric()
        }),
        body: t.Object({
            
        })
    })
   .listen(3000, () => {
    console.log(`Server is running at http://localhost:3000`);
   })